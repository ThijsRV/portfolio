﻿class stickyHeader {

    private _module: JQuery;
    private _menutoggle: JQuery;

    constructor() {
        this._module = $(".navigation");
        this._menutoggle = $(".menutoggle");
        this.addEventListener();
    }

    addEventListener() {
        $(window).scroll((event) => this.windowScrollHandler(event));
    }

    windowScrollHandler(event) {
        const scrollTop = $(window).scrollTop();


        if (scrollTop > 400) {

            this._module.addClass('is-sticky');
            this._menutoggle.addClass('is-sticky');

        } else {

            this._module.removeClass('is-sticky');
            this._menutoggle.removeClass('is-sticky');
        }
    }
}

const stickyheader = new stickyHeader();