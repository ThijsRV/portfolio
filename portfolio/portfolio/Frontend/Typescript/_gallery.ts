﻿class gallery {
    private _module: JQuery;
    private _imagetoggler: JQuery;
    private _imagesrc: JQuery;

    constructor(module) {

        this._module = module
        this._imagetoggler = $(".image-holder");
        this._imagesrc = $(".image-thumb");

        this.addEventListener(event);
    }

    addEventListener(e) {

        $(this._imagetoggler).on("click", (event) => this.toggleClass(event));

    }

    toggleClass(event) {

        const _imagetoggler = $(event.currentTarget);
        const currentImage = _imagetoggler.children('img').attr('src');

        _imagetoggler.toggleClass('overlay');
    }
}

const galleryimage = new gallery($(".gallery-section"));