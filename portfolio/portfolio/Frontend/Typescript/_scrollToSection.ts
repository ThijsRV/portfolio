﻿class pageScroller {

    constructor() {
        this.addEventListener();
    }

    addEventListener() {
        $('[data-section]').on("click", (event) => this.scrollToSection(event));
    }

    scrollToSection(event) {
    const className = $(event.currentTarget).attr('data-section');

    $('html, body').animate({
        scrollTop: $("." + className).offset().top
    }, 400);
     
    }
}
var scroller = new pageScroller();