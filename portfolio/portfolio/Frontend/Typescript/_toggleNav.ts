﻿class navigationToggle {

    private _module: JQuery;
    private _toggle: JQuery;
    private _body: JQuery;

    constructor() {
        this._body = $("body");
        this._module = $(".navigation");
        this._toggle = $(".menutoggle");

        this.addEventListener();
    }

    addEventListener() {
        $(this._toggle).on("click", (event) => this.toggleClass(event));
    }

    toggleClass(event) {

        this._module.toggleClass('active');
        this._body.toggleClass('no-scroll');
        
    }
}

const togglenav = new navigationToggle();