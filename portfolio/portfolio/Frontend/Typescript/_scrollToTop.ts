﻿class scrollToTop {
    _scrollButton: JQuery;
    _body: JQuery;

    constructor() {
        this._scrollButton = $(".totop-button");
        this._body = $("body");
        this.addEventListener(event);
    }

    addEventListener(event) {
        this._scrollButton.on("click", (event) => this.BackToTop(event));
        $(window).on("scroll", (event) => this.showScroll(event));
    }

    showScroll(event) {
        if ($(this).scrollTop() > 100) {
            this._scrollButton.fadeIn();
        }

        if ($(window).scrollTop() >= 500) {

            this._scrollButton.addClass('displayed');

        } else {

            this._scrollButton.removeClass('displayed');

        }
    }

    BackToTop(event) {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
    }
}

const scrollButton = new scrollToTop();