﻿class ViewportAnimation {

    _module: JQuery;
    constructor() {
        this._module = $('.work');
        this.addEventListener(event);
    }

    addEventListener(event) {
        $(window).on("scroll load", event => this.showScroll(event));
    }

    showScroll(event) {
        const viewportTop = $(window).scrollTop();
        const viewportBottom = $(window).scrollTop() + $(window).height();

        this._module.each(function () {
            const curr = $(this);
            const top = curr.offset().top;

            const bottom = top + curr.height();
            const topOfScreen = top >= viewportTop && top <= viewportBottom;
            const bottomOfScreen = bottom >= viewportTop && bottom <= viewportBottom;

            const elementVisible = topOfScreen || bottomOfScreen;

            if (elementVisible) {
                curr.addClass('animate');
            }
        });
    }
}

const animation = new ViewportAnimation();