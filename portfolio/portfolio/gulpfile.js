﻿'use strict';

// Requirements
var gulp = require('gulp'),
    clean = require('gulp-clean'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    typescript = require('gulp-tsc'),
    runSequence = require('run-sequence');

// Paths
var DEV = './Frontend',
	STYLES = '/scss',
	VENDORS = '/vendors',
	SCRIPTS = '/typescript';

// Default task
gulp.task('default', function () {
	runSequence('clean', ['sass', 'typescript']);
});

// Watch task
gulp.task('watch', function () {
	gulp.watch(DEV + STYLES + '/**/*.scss', ['sass']);
	gulp.watch(DEV + SCRIPTS + '/**/*.ts', ['typescript']);
});

// Clean task
gulp.task('clean', function () {
	return gulp.src(['./Css/Build', './Scripts/Build'], {read: false})
	.pipe(clean());
});

// Sass task
gulp.task('sass', function () {
    return gulp.src(DEV + STYLES + '/core.scss')
    .pipe(plumber())
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({ browsers: ['last 2 versions', '> 2%'], cascade: false }))
	.pipe(rename('core.css'))
	//.pipe(cssnano())
	//.pipe(rename('core.min.css'))
    .pipe(plumber.stop())
	.pipe(gulp.dest('./Css/Build'));
});

// Typescript task
gulp.task('typescript', function () {
    return gulp.src(DEV + SCRIPTS + '/**/*.ts')
    .pipe(plumber())
	.pipe(typescript({
        module: 'amd',
	    target: 'ES5',
	    inlineSourceMap: true,  //todo: remove for production
	    inlineSources: true,    //todo: remove for production
	    out: 'core.js'
	}))
	//.pipe(uglify())
    .pipe(plumber.stop())
	.pipe(gulp.dest('./Scripts/Build'));
});

