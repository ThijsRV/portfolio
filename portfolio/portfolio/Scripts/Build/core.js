var gallery = /** @class */ (function () {
    function gallery(module) {
        this._module = module;
        this._imagetoggler = $(".image-holder");
        this._imagesrc = $(".image-thumb");
        this.addEventListener(event);
    }
    gallery.prototype.addEventListener = function (e) {
        var _this = this;
        $(this._imagetoggler).on("click", function (event) { return _this.toggleClass(event); });
    };
    gallery.prototype.toggleClass = function (event) {
        var _imagetoggler = $(event.currentTarget);
        var currentImage = _imagetoggler.children('img').attr('src');
        _imagetoggler.toggleClass('overlay');
    };
    return gallery;
}());
var galleryimage = new gallery($(".gallery-section"));
var ViewportAnimation = /** @class */ (function () {
    function ViewportAnimation() {
        this._module = $('.work');
        this.addEventListener(event);
    }
    ViewportAnimation.prototype.addEventListener = function (event) {
        var _this = this;
        $(window).on("scroll load", function (event) { return _this.showScroll(event); });
    };
    ViewportAnimation.prototype.showScroll = function (event) {
        var viewportTop = $(window).scrollTop();
        var viewportBottom = $(window).scrollTop() + $(window).height();
        this._module.each(function () {
            var curr = $(this);
            var top = curr.offset().top;
            var bottom = top + curr.height();
            var topOfScreen = top >= viewportTop && top <= viewportBottom;
            var bottomOfScreen = bottom >= viewportTop && bottom <= viewportBottom;
            var elementVisible = topOfScreen || bottomOfScreen;
            if (elementVisible) {
                curr.addClass('animate');
            }
        });
    };
    return ViewportAnimation;
}());
var animation = new ViewportAnimation();
//class preLoader {
//    private _body: JQuery;
//    constructor() {
//        this._body = $('body');
//        this.addEventListener();
//        this.startPreload();
//    }
//    addEventListener() {
//        $(window).on("load", event => this.removepreLoader());
//    }
//    startPreload() {
//        this._body.addClass('no-scroll preload');
//    }
//    removepreLoader() {
//        setTimeout(function () {
//            $(".overlay").fadeOut("slow", function () {
//                $('body').removeClass('no-scroll preload');
//            });
//        }, 6000);
//    }
//}
//var preload = new preLoader(); 
var pageScroller = /** @class */ (function () {
    function pageScroller() {
        this.addEventListener();
    }
    pageScroller.prototype.addEventListener = function () {
        var _this = this;
        $('[data-section]').on("click", function (event) { return _this.scrollToSection(event); });
    };
    pageScroller.prototype.scrollToSection = function (event) {
        var className = $(event.currentTarget).attr('data-section');
        $('html, body').animate({
            scrollTop: $("." + className).offset().top
        }, 400);
    };
    return pageScroller;
}());
var scroller = new pageScroller();
var scrollToTop = /** @class */ (function () {
    function scrollToTop() {
        this._scrollButton = $(".totop-button");
        this._body = $("body");
        this.addEventListener(event);
    }
    scrollToTop.prototype.addEventListener = function (event) {
        var _this = this;
        this._scrollButton.on("click", function (event) { return _this.BackToTop(event); });
        $(window).on("scroll", function (event) { return _this.showScroll(event); });
    };
    scrollToTop.prototype.showScroll = function (event) {
        if ($(this).scrollTop() > 100) {
            this._scrollButton.fadeIn();
        }
        if ($(window).scrollTop() >= 500) {
            this._scrollButton.addClass('displayed');
        }
        else {
            this._scrollButton.removeClass('displayed');
        }
    };
    scrollToTop.prototype.BackToTop = function (event) {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
    };
    return scrollToTop;
}());
var scrollButton = new scrollToTop();
var stickyHeader = /** @class */ (function () {
    function stickyHeader() {
        this._module = $(".navigation");
        this._menutoggle = $(".menutoggle");
        this.addEventListener();
    }
    stickyHeader.prototype.addEventListener = function () {
        var _this = this;
        $(window).scroll(function (event) { return _this.windowScrollHandler(event); });
    };
    stickyHeader.prototype.windowScrollHandler = function (event) {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 400) {
            this._module.addClass('is-sticky');
            this._menutoggle.addClass('is-sticky');
        }
        else {
            this._module.removeClass('is-sticky');
            this._menutoggle.removeClass('is-sticky');
        }
    };
    return stickyHeader;
}());
var stickyheader = new stickyHeader();
var navigationToggle = /** @class */ (function () {
    function navigationToggle() {
        this._body = $("body");
        this._module = $(".navigation");
        this._toggle = $(".menutoggle");
        this.addEventListener();
    }
    navigationToggle.prototype.addEventListener = function () {
        var _this = this;
        $(this._toggle).on("click", function (event) { return _this.toggleClass(event); });
    };
    navigationToggle.prototype.toggleClass = function (event) {
        this._module.toggleClass('active');
        this._body.toggleClass('no-scroll');
    };
    return navigationToggle;
}());
var togglenav = new navigationToggle();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL0Zyb250ZW5kL3R5cGVzY3JpcHQvX2dhbGxlcnkudHMiLCIuLi9Gcm9udGVuZC90eXBlc2NyaXB0L19vbkxvYWRBbmltYXRpb24udHMiLCIuLi9Gcm9udGVuZC90eXBlc2NyaXB0L19QcmVsb2FkZXIudHMiLCIuLi9Gcm9udGVuZC90eXBlc2NyaXB0L19zY3JvbGxUb1NlY3Rpb24udHMiLCIuLi9Gcm9udGVuZC90eXBlc2NyaXB0L19zY3JvbGxUb1RvcC50cyIsIi4uL0Zyb250ZW5kL3R5cGVzY3JpcHQvX3N0aWNreUhlYWRlci50cyIsIi4uL0Zyb250ZW5kL3R5cGVzY3JpcHQvX3RvZ2dsZU5hdi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUtJLGlCQUFZLE1BQU07UUFFZCxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQTtRQUNyQixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUVuQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELGtDQUFnQixHQUFoQixVQUFpQixDQUFDO1FBQWxCLGlCQUlDO1FBRkcsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO0lBRTFFLENBQUM7SUFFRCw2QkFBVyxHQUFYLFVBQVksS0FBSztRQUViLElBQU0sYUFBYSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDN0MsSUFBTSxZQUFZLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFL0QsYUFBYSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBQ0wsY0FBQztBQUFELENBQUMsQUEzQkQsSUEyQkM7QUFFRCxJQUFNLFlBQVksR0FBRyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO0FDN0J4RDtJQUdJO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCw0Q0FBZ0IsR0FBaEIsVUFBaUIsS0FBSztRQUF0QixpQkFFQztRQURHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFRCxzQ0FBVSxHQUFWLFVBQVcsS0FBSztRQUNaLElBQU0sV0FBVyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMxQyxJQUFNLGNBQWMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRWxFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2QsSUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JCLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFFOUIsSUFBTSxNQUFNLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNuQyxJQUFNLFdBQVcsR0FBRyxHQUFHLElBQUksV0FBVyxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUM7WUFDaEUsSUFBTSxjQUFjLEdBQUcsTUFBTSxJQUFJLFdBQVcsSUFBSSxNQUFNLElBQUksY0FBYyxDQUFDO1lBRXpFLElBQU0sY0FBYyxHQUFHLFdBQVcsSUFBSSxjQUFjLENBQUM7WUFFckQsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3QixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wsd0JBQUM7QUFBRCxDQUFDLEFBL0JELElBK0JDO0FBRUQsSUFBTSxTQUFTLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO0FDakMxQyxtQkFBbUI7QUFDbkIsNEJBQTRCO0FBRTVCLHFCQUFxQjtBQUNyQixpQ0FBaUM7QUFDakMsa0NBQWtDO0FBQ2xDLDhCQUE4QjtBQUM5QixPQUFPO0FBRVAsMEJBQTBCO0FBQzFCLGdFQUFnRTtBQUNoRSxPQUFPO0FBR1Asc0JBQXNCO0FBQ3RCLG1EQUFtRDtBQUNuRCxPQUFPO0FBRVAseUJBQXlCO0FBQ3pCLGtDQUFrQztBQUNsQyx5REFBeUQ7QUFDekQsNkRBQTZEO0FBQzdELGlCQUFpQjtBQUNqQixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLEdBQUc7QUFFSCxnQ0FBZ0M7QUMzQmhDO0lBRUk7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsdUNBQWdCLEdBQWhCO1FBQUEsaUJBRUM7UUFERyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFRCxzQ0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDckIsSUFBTSxTQUFTLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFOUQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNwQixTQUFTLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHO1NBQzdDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFFUixDQUFDO0lBQ0wsbUJBQUM7QUFBRCxDQUFDLEFBbEJELElBa0JDO0FBQ0QsSUFBSSxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztBQ25CbEM7SUFJSTtRQUNJLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsc0NBQWdCLEdBQWhCLFVBQWlCLEtBQUs7UUFBdEIsaUJBR0M7UUFGRyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFyQixDQUFxQixDQUFDLENBQUM7UUFDakUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUF0QixDQUFzQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELGdDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ1osRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNoQyxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFFL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFN0MsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBRUosSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFaEQsQ0FBQztJQUNMLENBQUM7SUFFRCwrQkFBUyxHQUFULFVBQVUsS0FBSztRQUNYLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFDbkIsU0FBUyxFQUFFLENBQUM7U0FDZixFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUNMLGtCQUFDO0FBQUQsQ0FBQyxBQXBDRCxJQW9DQztBQUVELElBQU0sWUFBWSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7QUN0Q3ZDO0lBS0k7UUFDSSxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsdUNBQWdCLEdBQWhCO1FBQUEsaUJBRUM7UUFERyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxFQUEvQixDQUErQixDQUFDLENBQUM7SUFDakUsQ0FBQztJQUVELDBDQUFtQixHQUFuQixVQUFvQixLQUFLO1FBQ3JCLElBQU0sU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUd4QyxFQUFFLENBQUMsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUVsQixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUUzQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFFSixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5QyxDQUFDO0lBQ0wsQ0FBQztJQUNMLG1CQUFDO0FBQUQsQ0FBQyxBQTlCRCxJQThCQztBQUVELElBQU0sWUFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7QUNoQ3hDO0lBTUk7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUVoQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsMkNBQWdCLEdBQWhCO1FBQUEsaUJBRUM7UUFERyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUF2QixDQUF1QixDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVELHNDQUFXLEdBQVgsVUFBWSxLQUFLO1FBRWIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7SUFFeEMsQ0FBQztJQUNMLHVCQUFDO0FBQUQsQ0FBQyxBQXhCRCxJQXdCQztBQUVELElBQU0sU0FBUyxHQUFHLElBQUksZ0JBQWdCLEVBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImNsYXNzIGdhbGxlcnkge1xyXG4gICAgcHJpdmF0ZSBfbW9kdWxlOiBKUXVlcnk7XHJcbiAgICBwcml2YXRlIF9pbWFnZXRvZ2dsZXI6IEpRdWVyeTtcclxuICAgIHByaXZhdGUgX2ltYWdlc3JjOiBKUXVlcnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IobW9kdWxlKSB7XHJcblxyXG4gICAgICAgIHRoaXMuX21vZHVsZSA9IG1vZHVsZVxyXG4gICAgICAgIHRoaXMuX2ltYWdldG9nZ2xlciA9ICQoXCIuaW1hZ2UtaG9sZGVyXCIpO1xyXG4gICAgICAgIHRoaXMuX2ltYWdlc3JjID0gJChcIi5pbWFnZS10aHVtYlwiKTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRFdmVudExpc3RlbmVyKGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRFdmVudExpc3RlbmVyKGUpIHtcclxuXHJcbiAgICAgICAgJCh0aGlzLl9pbWFnZXRvZ2dsZXIpLm9uKFwiY2xpY2tcIiwgKGV2ZW50KSA9PiB0aGlzLnRvZ2dsZUNsYXNzKGV2ZW50KSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUNsYXNzKGV2ZW50KSB7XHJcblxyXG4gICAgICAgIGNvbnN0IF9pbWFnZXRvZ2dsZXIgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRJbWFnZSA9IF9pbWFnZXRvZ2dsZXIuY2hpbGRyZW4oJ2ltZycpLmF0dHIoJ3NyYycpO1xyXG5cclxuICAgICAgICBfaW1hZ2V0b2dnbGVyLnRvZ2dsZUNsYXNzKCdvdmVybGF5Jyk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IGdhbGxlcnlpbWFnZSA9IG5ldyBnYWxsZXJ5KCQoXCIuZ2FsbGVyeS1zZWN0aW9uXCIpKTsiLCJjbGFzcyBWaWV3cG9ydEFuaW1hdGlvbiB7XHJcblxyXG4gICAgX21vZHVsZTogSlF1ZXJ5O1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5fbW9kdWxlID0gJCgnLndvcmsnKTtcclxuICAgICAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZEV2ZW50TGlzdGVuZXIoZXZlbnQpIHtcclxuICAgICAgICAkKHdpbmRvdykub24oXCJzY3JvbGwgbG9hZFwiLCBldmVudCA9PiB0aGlzLnNob3dTY3JvbGwoZXZlbnQpKTtcclxuICAgIH1cclxuXHJcbiAgICBzaG93U2Nyb2xsKGV2ZW50KSB7XHJcbiAgICAgICAgY29uc3Qgdmlld3BvcnRUb3AgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCk7XHJcbiAgICAgICAgY29uc3Qgdmlld3BvcnRCb3R0b20gPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgKyAkKHdpbmRvdykuaGVpZ2h0KCk7XHJcblxyXG4gICAgICAgIHRoaXMuX21vZHVsZS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgY29uc3QgY3VyciA9ICQodGhpcyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHRvcCA9IGN1cnIub2Zmc2V0KCkudG9wO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYm90dG9tID0gdG9wICsgY3Vyci5oZWlnaHQoKTtcclxuICAgICAgICAgICAgY29uc3QgdG9wT2ZTY3JlZW4gPSB0b3AgPj0gdmlld3BvcnRUb3AgJiYgdG9wIDw9IHZpZXdwb3J0Qm90dG9tO1xyXG4gICAgICAgICAgICBjb25zdCBib3R0b21PZlNjcmVlbiA9IGJvdHRvbSA+PSB2aWV3cG9ydFRvcCAmJiBib3R0b20gPD0gdmlld3BvcnRCb3R0b207XHJcblxyXG4gICAgICAgICAgICBjb25zdCBlbGVtZW50VmlzaWJsZSA9IHRvcE9mU2NyZWVuIHx8IGJvdHRvbU9mU2NyZWVuO1xyXG5cclxuICAgICAgICAgICAgaWYgKGVsZW1lbnRWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgICAgICBjdXJyLmFkZENsYXNzKCdhbmltYXRlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5cclxuY29uc3QgYW5pbWF0aW9uID0gbmV3IFZpZXdwb3J0QW5pbWF0aW9uKCk7IiwiLy9jbGFzcyBwcmVMb2FkZXIge1xyXG4vLyAgICBwcml2YXRlIF9ib2R5OiBKUXVlcnk7XHJcblxyXG4vLyAgICBjb25zdHJ1Y3RvcigpIHtcclxuLy8gICAgICAgIHRoaXMuX2JvZHkgPSAkKCdib2R5Jyk7XHJcbi8vICAgICAgICB0aGlzLmFkZEV2ZW50TGlzdGVuZXIoKTtcclxuLy8gICAgICAgIHRoaXMuc3RhcnRQcmVsb2FkKCk7XHJcbi8vICAgIH1cclxuXHJcbi8vICAgIGFkZEV2ZW50TGlzdGVuZXIoKSB7XHJcbi8vICAgICAgICAkKHdpbmRvdykub24oXCJsb2FkXCIsIGV2ZW50ID0+IHRoaXMucmVtb3ZlcHJlTG9hZGVyKCkpO1xyXG4vLyAgICB9XHJcblxyXG5cclxuLy8gICAgc3RhcnRQcmVsb2FkKCkge1xyXG4vLyAgICAgICAgdGhpcy5fYm9keS5hZGRDbGFzcygnbm8tc2Nyb2xsIHByZWxvYWQnKTtcclxuLy8gICAgfVxyXG5cclxuLy8gICAgcmVtb3ZlcHJlTG9hZGVyKCkge1xyXG4vLyAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbi8vICAgICAgICAgICAgJChcIi5vdmVybGF5XCIpLmZhZGVPdXQoXCJzbG93XCIsIGZ1bmN0aW9uICgpIHtcclxuLy8gICAgICAgICAgICAgICAgJCgnYm9keScpLnJlbW92ZUNsYXNzKCduby1zY3JvbGwgcHJlbG9hZCcpO1xyXG4vLyAgICAgICAgICAgIH0pO1xyXG4vLyAgICAgICAgfSwgNjAwMCk7XHJcbi8vICAgIH1cclxuLy99XHJcblxyXG4vL3ZhciBwcmVsb2FkID0gbmV3IHByZUxvYWRlcigpOyIsImNsYXNzIHBhZ2VTY3JvbGxlciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5hZGRFdmVudExpc3RlbmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkRXZlbnRMaXN0ZW5lcigpIHtcclxuICAgICAgICAkKCdbZGF0YS1zZWN0aW9uXScpLm9uKFwiY2xpY2tcIiwgKGV2ZW50KSA9PiB0aGlzLnNjcm9sbFRvU2VjdGlvbihldmVudCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHNjcm9sbFRvU2VjdGlvbihldmVudCkge1xyXG4gICAgY29uc3QgY2xhc3NOYW1lID0gJChldmVudC5jdXJyZW50VGFyZ2V0KS5hdHRyKCdkYXRhLXNlY3Rpb24nKTtcclxuXHJcbiAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XHJcbiAgICAgICAgc2Nyb2xsVG9wOiAkKFwiLlwiICsgY2xhc3NOYW1lKS5vZmZzZXQoKS50b3BcclxuICAgIH0sIDQwMCk7XHJcbiAgICAgXHJcbiAgICB9XHJcbn1cclxudmFyIHNjcm9sbGVyID0gbmV3IHBhZ2VTY3JvbGxlcigpOyIsImNsYXNzIHNjcm9sbFRvVG9wIHtcclxuICAgIF9zY3JvbGxCdXR0b246IEpRdWVyeTtcclxuICAgIF9ib2R5OiBKUXVlcnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5fc2Nyb2xsQnV0dG9uID0gJChcIi50b3RvcC1idXR0b25cIik7XHJcbiAgICAgICAgdGhpcy5fYm9keSA9ICQoXCJib2R5XCIpO1xyXG4gICAgICAgIHRoaXMuYWRkRXZlbnRMaXN0ZW5lcihldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkRXZlbnRMaXN0ZW5lcihldmVudCkge1xyXG4gICAgICAgIHRoaXMuX3Njcm9sbEJ1dHRvbi5vbihcImNsaWNrXCIsIChldmVudCkgPT4gdGhpcy5CYWNrVG9Ub3AoZXZlbnQpKTtcclxuICAgICAgICAkKHdpbmRvdykub24oXCJzY3JvbGxcIiwgKGV2ZW50KSA9PiB0aGlzLnNob3dTY3JvbGwoZXZlbnQpKTtcclxuICAgIH1cclxuXHJcbiAgICBzaG93U2Nyb2xsKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKCQodGhpcykuc2Nyb2xsVG9wKCkgPiAxMDApIHtcclxuICAgICAgICAgICAgdGhpcy5fc2Nyb2xsQnV0dG9uLmZhZGVJbigpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCQod2luZG93KS5zY3JvbGxUb3AoKSA+PSA1MDApIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3Njcm9sbEJ1dHRvbi5hZGRDbGFzcygnZGlzcGxheWVkJyk7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9zY3JvbGxCdXR0b24ucmVtb3ZlQ2xhc3MoJ2Rpc3BsYXllZCcpO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQmFja1RvVG9wKGV2ZW50KSB7XHJcbiAgICAgICAgJCgnYm9keSxodG1sJykuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgIHNjcm9sbFRvcDogMFxyXG4gICAgICAgIH0sIDQwMCk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IHNjcm9sbEJ1dHRvbiA9IG5ldyBzY3JvbGxUb1RvcCgpOyIsImNsYXNzIHN0aWNreUhlYWRlciB7XHJcblxyXG4gICAgcHJpdmF0ZSBfbW9kdWxlOiBKUXVlcnk7XHJcbiAgICBwcml2YXRlIF9tZW51dG9nZ2xlOiBKUXVlcnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5fbW9kdWxlID0gJChcIi5uYXZpZ2F0aW9uXCIpO1xyXG4gICAgICAgIHRoaXMuX21lbnV0b2dnbGUgPSAkKFwiLm1lbnV0b2dnbGVcIik7XHJcbiAgICAgICAgdGhpcy5hZGRFdmVudExpc3RlbmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkRXZlbnRMaXN0ZW5lcigpIHtcclxuICAgICAgICAkKHdpbmRvdykuc2Nyb2xsKChldmVudCkgPT4gdGhpcy53aW5kb3dTY3JvbGxIYW5kbGVyKGV2ZW50KSk7XHJcbiAgICB9XHJcblxyXG4gICAgd2luZG93U2Nyb2xsSGFuZGxlcihldmVudCkge1xyXG4gICAgICAgIGNvbnN0IHNjcm9sbFRvcCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcclxuXHJcblxyXG4gICAgICAgIGlmIChzY3JvbGxUb3AgPiA0MDApIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX21vZHVsZS5hZGRDbGFzcygnaXMtc3RpY2t5Jyk7XHJcbiAgICAgICAgICAgIHRoaXMuX21lbnV0b2dnbGUuYWRkQ2xhc3MoJ2lzLXN0aWNreScpO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fbW9kdWxlLnJlbW92ZUNsYXNzKCdpcy1zdGlja3knKTtcclxuICAgICAgICAgICAgdGhpcy5fbWVudXRvZ2dsZS5yZW1vdmVDbGFzcygnaXMtc3RpY2t5Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBzdGlja3loZWFkZXIgPSBuZXcgc3RpY2t5SGVhZGVyKCk7IiwiY2xhc3MgbmF2aWdhdGlvblRvZ2dsZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBfbW9kdWxlOiBKUXVlcnk7XHJcbiAgICBwcml2YXRlIF90b2dnbGU6IEpRdWVyeTtcclxuICAgIHByaXZhdGUgX2JvZHk6IEpRdWVyeTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLl9ib2R5ID0gJChcImJvZHlcIik7XHJcbiAgICAgICAgdGhpcy5fbW9kdWxlID0gJChcIi5uYXZpZ2F0aW9uXCIpO1xyXG4gICAgICAgIHRoaXMuX3RvZ2dsZSA9ICQoXCIubWVudXRvZ2dsZVwiKTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRFdmVudExpc3RlbmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkRXZlbnRMaXN0ZW5lcigpIHtcclxuICAgICAgICAkKHRoaXMuX3RvZ2dsZSkub24oXCJjbGlja1wiLCAoZXZlbnQpID0+IHRoaXMudG9nZ2xlQ2xhc3MoZXZlbnQpKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVDbGFzcyhldmVudCkge1xyXG5cclxuICAgICAgICB0aGlzLl9tb2R1bGUudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgIHRoaXMuX2JvZHkudG9nZ2xlQ2xhc3MoJ25vLXNjcm9sbCcpO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCB0b2dnbGVuYXYgPSBuZXcgbmF2aWdhdGlvblRvZ2dsZSgpOyJdfQ==